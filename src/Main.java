import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main
	implements ActionListener {

	/* UI Elements */
	JFrame mainFrame;
	JButton pollButton;
	JScrollPane ipTableScrollPane;
	JTable ipTable;
	
	/* Data members */
	Connection conn;
	private static String ConnectionString = "jdbc:sqlserver://DATABASE_SERVER:1433;user=YOUR_USERNAME;password=YOUR_PASSWORD;databaseName=YOUR_DATABASE";
	
	/* Miscellaneous */
	private static enum ActionCommands {
		PollTheIPs
	}
	
	List<Object[]> ipData = new ArrayList<Object[]>();
	
	DefaultTableModel ipTableModel;
	
	String[] ipTableColumns = {
		"Device IP",
		"Status"
    };
	
	// Constructor
	public Main() {
		// initialize the window
		mainFrame = new JFrame();
		mainFrame.setSize(800, 600);
		mainFrame.setLocation(500, 200);
		mainFrame.setTitle("Hello World - SQL JDBC Integration");
		mainFrame.setLayout(null);
		mainFrame.setVisible(true);
		
		// setup the poll button
		pollButton = new JButton();
		pollButton.setText("Get IP List");
		pollButton.setBounds(15, 15, 150, 40);
		pollButton.addActionListener(this);
		pollButton.setActionCommand(ActionCommands.PollTheIPs.toString());
		mainFrame.add(pollButton);
		
		// setup the IP table
		ipTableModel = new DefaultTableModel(ipTableColumns, 0);
		ipTable = new JTable();
		ipTable.setModel(ipTableModel);
		
		// setup the IP table scroll pane
		ipTableScrollPane = new JScrollPane(ipTable);
		ipTable.setFillsViewportHeight(true);
		ipTableScrollPane.setBounds(15, 70, 755, 400);
		ipTableScrollPane.setOpaque(true);
		mainFrame.add(ipTableScrollPane);
		
		// connect to database
		try {
			// initialize driver
			System.out.println("Initializing com.microsoft.sqlserver.jdbc.SQLServerDriver ...");
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			System.out.println("Driver Initialized!");
			
			// connect to [bws_test] database
			System.out.println("Connecting to database ...");
			conn = DriverManager.getConnection(ConnectionString);
			System.out.println("Connected!");
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(0);
		}
		
		// status message
		System.out.println("Ready.");
	}
	
	public void getIpList() {
		try {
			System.out.println("Polling IP list ...");
			
			// empty the rows in the model
			ipTableModel.setRowCount(0);
			
			// run test query
			Statement stmt = conn.createStatement();
			ResultSet res = stmt.executeQuery("SELECT reg.[DeviceIp], reg.[Status] FROM [Registration] reg ORDER BY reg.[DeviceIp]");
			
			// loop through the IPs
			while (res.next()) {				
				ipTableModel.addRow(new Object[] { res.getString("DeviceIp"), res.getString("Status") });
			}
			
			// log result
			System.out.println("List Polled!");
		}
		catch (Exception ex) {
			System.err.println(ex.getMessage());
		}
	}
	
	public static void main(String[] args) {
		// Invoke constructor
		Main m = new Main();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == ActionCommands.PollTheIPs.toString()) {
			getIpList();
		}
	}
}
